// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Header from './Header/Header';
import Menu from './Menu Tab/Menu';
// import Appnavbar from './Appnavbar'
import "./App.css";
import { Routes , Route } from 'react-router-dom';
import Briefing from './component/briefing';



function App() {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="/" element={<Menu />}/>
        <Route path="/briefing" element={<Briefing />}/>
      </Routes>
    </div>
  );
}

export default App;
