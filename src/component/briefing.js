import React from 'react';
import { Typography } from '@mui/material'; 
import Box from '@mui/material/Box';
import { createTheme , ThemeProvider } from '@mui/material';
import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
// bikin tombol search
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
// import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';

import { Container } from '@mui/system';

// icon kamera
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import { TextField } from '@mui/material';
import { Link } from 'react-router-dom';





// buat kotak dibagian tengah
// import { Card, CardActionArea, CardMedia, CardContent} from '@mui/material';



 
const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }
});


function Menu() {
    return (
        <ThemeProvider theme={theme}>
            <Box bgcolor='#303F51'>
                    <Container maxWidth="xl">
                        <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Briefing</Typography>
                    </Container>
                    <Box>
                        <Grid container spacing={1} justifyContent='center'>
                            <Button component={Link} to='/'>   
                            <Grid item>
                                <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                    <Typography variant='subtitle2'>Rute Operasional</Typography>
                                   
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2' color='#ECAB55'>Briefing</Typography>
                            </Grid>
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Go/No Go Item</Typography>
                            </Grid> 
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>

                            <Button>  
                            <Grid item>
                                <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Riwayat Catatan</Typography>
                            </Grid>   
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                            </Button>

                            <Button>
                            <Grid item>
                                <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                               
                            </Grid>  
                            <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                            </Button>
                            
                            <Button> 
                            <Grid item>
                                <WarningAmberRoundedIcon color='primary'sx={{width: 60 , height: 60 , margin: 3.5}} />
                                <Typography variant='subtitle2'>Bantuan Darurat</Typography>
                            </Grid>
                            </Button>
                        </Grid>
                    </Box>
                       
                        <Grid container justifyContent='center'>
                            {/* tombol search */}
                            <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, marginTop: 2 }}>
                                {/* <IconButton sx={{ p: '10px' }} aria-label="menu">
                                    <MenuIcon />
                                </IconButton> */}
                                <InputBase
                                    sx={{ ml: 1, flex: 1 }}
                                    placeholder="Pencarian"
                                    inputProps={{ 'aria-label': 'Pencarian' }}
                                />
                                    <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                        <SearchIcon/>
                                    </IconButton>

                            </Paper>
                        </Grid>

                    <Box>
                        <Typography 
                        sx={{fontFamily: 'Montserrat', marginTop: 4}} 
                        color='white'
                        align='center'
                        >RUTE OPERASIONAL</Typography> 
                    </Box>   
                    
                    {/* bagian tengah judul */}
                    <Container maxWidth="xl">
                    <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
                            <Typography sx={{fontWeight: 400}} color='white'>Senin, 02 Jan 2022</Typography> 
                            <Typography sx={{fontWeight: 400}} color='white'>Kalideres (KDL 023) - Ponogoro (PNG 013)</Typography> 
                        <Box sx={{display: 'flex'}}> 
                            <Typography sx={{fontWeight: 400}} color='white'>No.Bus</Typography> 
                            <Typography sx={{ml: 1, fontFamily: 'Montserrat'}} color='white'>BM 007</Typography> 
                        </Box>
                    </Box>
                    <Divider sx={{borderColor: 'white', pt: 2}}/> 



                    {/* Membuat 3 Baris Kotak Besar */}
                    <Grid container pt={2} spacing={1}>
                        <Grid item xs >
                            <Box bgcolor='white' sx={{width: 425, height: 348}}>
                                <Button justify='center' sx={{width: 425, height: 348}}>
                                <AddAPhotoIcon sx={{color: '#D2D2D2', width: 70 , height: 70 }}/>
                                </Button>
                            </Box>
                        </Grid>

                        <Grid item xs>
                            <Box bgcolor='white'sx={{width: 425, height: 348}}>
                                <Button justify='center' sx={{width: 425, height: 348}}>
                                <AddAPhotoIcon sx={{color: '#D2D2D2', width: 70 , height: 70 }}/>
                                </Button>  
                            </Box>
                        </Grid>

                        <Grid item xs>
                            <Box bgcolor='white'sx={{width: 425, height: 348}}>
                                <Button justify='center' sx={{width: 425, height: 348}}>
                                <AddAPhotoIcon sx={{color: '#D2D2D2', width: 70 , height: 70 }}/>
                                </Button>  
                            </Box>
                        </Grid>
                    </Grid>
                    </Container>  
                    

                    {/* Membuat Tampilan Catatan */}
                    <Container maxWidth="xl">
                    <Typography pt={2} color='white' align='center'>Catatan</Typography>
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        // rows={1}
                        maxRows={2}        
                        placeholder='Masukkan Catatan disini'
                        sx={{width: 1301, backgroundColor: 'white'}}  
                        InputProps={{
                            inputProps: {
                                style: { textAlign: "center" }, "&::placeholder":{textAlign: "center"}
                            }
                        }}
                    />
                    </Container>


                    <Container sx={{ml: 2}}>
                        <Box pt={4}>
                        <Button 
                            variant="contained"
                            color="success"
                            sx={{width: 1270, height: 80}}               
                            >
                            <Typography sx={{fontFamily: 'Montserrat'}}>Submit</Typography>
                        </Button>
                        </Box>
                        <Box>
                            <Typography color='transparent'>row</Typography>
                        </Box>
                    </Container>        
            </Box>
        </ThemeProvider>
    );
}

export default Menu;

