import React from 'react';
import { ThemeProvider } from '@mui/material';
import { Box } from '@mui/material';

import driver from '../assets/chauffeur.png';
import Grid from '@mui/material/Grid';
import briefing from '../assets/briefing.png';
import building from '../assets/building.png';
import notes from '../assets/notes.png';
import serah from '../assets/serahterima.png';
import { Container } from '@mui/system';
import { Typography } from '@mui/material';
import { Divider } from '@mui/material';
import { Button } from '@mui/material';
import WarningAmberRoundedIcon from '@mui/icons-material/WarningAmberRounded';
import { Paper } from '@mui/material';
import { InputBase } from '@mui/material';
import { IconButton } from '@mui/material';

import SearchIcon from '@mui/icons-material/Search';
import { createTheme } from '@mui/material';
import { grey } from '@mui/material/colors';


const theme = createTheme({
    palette: {
        primary: {
            main : grey[200],
        },
        // primary: lightBlue,
        // secondary: pink,
    }
});

function penyerahan(props) {
    return (
        <ThemeProvider theme={theme}>
        <Box bgcolor='#303F51'>
                <Container maxWidth="xl">
                    <Typography sx={{bgColor: 'primary'}} color='#FFFFFF'>Jadwal Operasional Supir / Rute Operasional</Typography>
                </Container>
                <Box>
                    <Grid container spacing={1} justifyContent='center'>
                        <Button>   
                        <Grid item>
                            <Box component={'img'} src={driver} sx={{width: 50 , height: 50 , margin: 4}}/>
                                <Typography variant='subtitle2' color='#ECAB55'>Rute Operasional</Typography>
                               
                        </Grid>
                        <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                        </Button>
                        
                        <Button> 
                        <Grid item>
                            <Box component={'img'} color='primary'src={briefing} sx={{width: 50 , height: 50 , margin: 4}}/>
                            <Typography variant='subtitle2'>Briefing</Typography>
                        </Grid>
                        <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                        </Button>

                        <Button>
                        <Grid item>
                            <Box component={'img'} src={building} sx={{width: 50 , height: 50 , margin: 4}}/>
                            <Typography variant='subtitle2'>Go/No Go Item</Typography>
                        </Grid> 
                        <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                        </Button>

                        <Button>  
                        <Grid item>
                            <Box component={'img'} src={notes} sx={{width: 50 , height: 50 , margin: 4}}/>
                            <Typography variant='subtitle2'>Riwayat Catatan</Typography>
                        </Grid>   
                        <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />

                        </Button>

                        <Button>
                        <Grid item>
                            <Box component={'img'} src={serah} sx={{width: 50 , height: 50 , margin: 4}}/>
                            <Typography variant='subtitle2'>Serah Terima Dinasan</Typography>
                           
                        </Grid>  
                        <Divider sx={{ height: 60, m: 0.5, marginTop: 4, borderColor: 'white'}} orientation="vertical" flexItem />
                        </Button>
                        
                        <Button> 
                        <Grid item>
                            <WarningAmberRoundedIcon color='primary'sx={{width: 60 , height: 60 , margin: 3.5}} />
                            <Typography variant='subtitle2'>Bantuan Darurat</Typography>
                        </Grid>
                        </Button>
                    </Grid>
                </Box>
                   
                    <Grid container justifyContent='center'>
                        {/* tombol search */}
                        <Paper component="form" sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, marginTop: 2 }}>
                            {/* <IconButton sx={{ p: '10px' }} aria-label="menu">
                                <MenuIcon />
                            </IconButton> */}
                            <InputBase
                                sx={{ ml: 1, flex: 1 }}
                                placeholder="Pencarian"
                                inputProps={{ 'aria-label': 'Pencarian' }}
                            />
                                <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                    <SearchIcon/>
                                </IconButton>

                        </Paper>
                    </Grid>

                <Box>
                    <Typography 
                    sx={{fontFamily: 'Montserrat', marginTop: 4}} 
                    color='white'
                    align='center'
                    >RUTE OPERASIONAL</Typography> 
                </Box>   

                <Container maxWidth="xl">
                <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
                        <Typography sx={{fontWeight: 400}} color='white'>Senin, 02 Jan 2023</Typography> 
                        <Typography sx={{fontWeight: 400}} color='white'>Kalideres (KDL 023) - Ponogoro (PNG 013)</Typography> 
                    <Box sx={{display: 'flex'}}> 
                        <Typography sx={{fontWeight: 400}} color='white'>No.Bus</Typography> 
                        <Typography sx={{ml: 1, fontFamily: 'Montserrat'}} color='white'>BM 007</Typography> 
                    </Box>
                </Box>
                </Container>
                <Divider sx={{borderColor: 'white', pt: 2}}/> 

        </Box>
        </ThemeProvider>
    );
}

export default penyerahan;